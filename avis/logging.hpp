// avis/logging.hpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
#ifndef AVIS_LOGGING_HPP_INCLUDED_5E7M6I
#define AVIS_LOGGING_HPP_INCLUDED_5E7M6I

// Define:
// AVIS_CHECK_SLOW_ENABLE to enable AVIS_CHECK_SLOW
// AVIS_CHECK_VERY_SLOW_ENABLE to enable AVIS_CHECK_SLOW and AVIS_CHECK_VERY_SLOW
// AVIS_LOG_TO_STDERR to log to stderr
// AVIS_LOG_FILENAME otherwise to select a file for logging to
// AVIS_LOG_FILE_APP to open the logging file in append mode
// NDEBUG to disable AVIS_LOG_DBG

#if !defined(AVIS_LOG_FILENAME) && !defined(AVIS_LOG_TO_STDERR)
  #define AVIS_LOG_FILENAME "avis.log"
#endif

#include <cstdlib> // exit
// For blocking log object in the case of concurrent access:
#include <mutex>
// For date and time output:
#include <ctime>
#include <iomanip>

#include <boost/noncopyable.hpp>

#ifdef AVIS_LOG_TO_STDERR
  #include <iostream>
#else
  #include <fstream>
#endif

namespace avis
{

  class Current_date_tag {}
  const current_date;

  class Current_time_tag {}
  const current_time;

  class Now_tag {}
  const now;

  inline std::ostream & operator<<(std::ostream & os, Current_date_tag)
  {
    std::time_t t = std::time(nullptr);
    std::tm lt = *std::localtime(&t);
    return os << std::put_time(&lt, "%Y %b %d");
  }

  inline std::ostream & operator<<(std::ostream & os, Current_time_tag)
  {
    std::time_t t = std::time(nullptr);
    std::tm lt = *std::localtime(&t);
    return os << std::put_time(&lt, "%H:%M:%S");
  }

  inline std::ostream & operator<<(std::ostream & os, Now_tag)
  {
    std::time_t t = std::time(nullptr);
    std::tm lt = *std::localtime(&t);
    return os << std::put_time(&lt, "%Y %b %d %H:%M:%S");
  }

  // Global log object.
  inline class : boost::noncopyable
  {
  public:
    // Check if the logging stream is good.
    bool is_valid() const noexcept
    {
      return !!_log;
    }
  
    // Enable logging.
    void on() noexcept { _enabled = true; }
    // Disable logging.
    void off() noexcept { _enabled = false; }

    // Output everything to log one by one.
    template <typename... Args>
    void operator()(Args&&... args)
    {
      if (_enabled)
      {
        std::lock_guard lock { _mut };
        ((_log << args) && ...);
        _log.flush();
      }
    }
  
  private:
    std::mutex _mut;
    bool _enabled = true;

  #ifdef AVIS_LOG_TO_STDERR
    std::ostream & _log = std::clog;
  #else
    std::ofstream _log 
    { 
      AVIS_LOG_FILENAME
    #ifdef AVIS_LOG_FILE_APP
    , std::io::app
    #endif
    };
  #endif
  } log;
  
}

// Always enabled.
#define AVIS_LOG(...) avis::log(__VA_ARGS__)

// Enabled only if no NDEBUG.
#ifdef NDEBUG
  #define AVIS_LOG_DBG(...) ((void)0)
#else
  #define AVIS_LOG_DBG(...) avis::log(__VA_ARGS__)
#endif

// Auxiliary macros for deferring stringize.
#define AVIS_STRINGIZE(x) AVIS_STRINGIZE2(x)
#define AVIS_STRINGIZE2(x) #x

// Always enabled, exits program on condition falsity.
#define AVIS_ASSERT(c, ...) \
  ((void)((c)? (void)0: (avis::log("In ", __FILE__, " line ", AVIS_STRINGIZE(__LINE__), ", assertion failure: ", #c, '\n', __VA_ARGS__), std::exit(-1))))

// Always enabled, logs a message on condition falsity.
#define AVIS_CHECK(c, ...) \
  ((void)((c)? (void)0: avis::log("In ", __FILE__, " line ", AVIS_STRINGIZE(__LINE__), ", check failed: ", #c, '\n', __VA_ARGS__, '\n')))

// If very slow checks are enabled then slow checks should be enabled too.
#if defined(AVIS_CHECK_VERY_SLOW_ENABLE) && !defined(AVIS_CHECK_SLOW_ENABLE)
  #define AVIS_CHECK_SLOW_ENABLE
#endif

// Slow checks are done only if they are enabled. 
// Otherwise they are no different from "simple" (fast) checks.
#ifdef AVIS_CHECK_SLOW_ENABLE
  #define AVIS_CHECK_SLOW(c, ...) AVIS_CHECK(c, __VA_ARGS__)
#else
  #define AVIS_CHECK_SLOW(c, ...) ((void)0)
#endif

// Very slow checks are done only if they are enabled. 
// Otherwise they are no different from "simple" (fast) checks.
#ifdef AVIS_CHECK_VERY_SLOW_ENABLE
  #define AVIS_CHECK_VERY_SLOW(c, m) AVIS_CHECK(c, __VA_ARGS__)
#else
  #define AVIS_CHECK_VERY_SLOW(c, m) ((void)0)
#endif

#endif//AVIS_LOGGING_HPP_INCLUDED_5E7M6I
