// avis/sideros.hpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
#ifndef AVIS_SIDEROS_HPP_INCLUDED_Q54UP0
#define AVIS_SIDEROS_HPP_INCLUDED_Q54UP0
// To enable Win32 console attachment:
//#define AVIS_WIN32_CONSOLE_ENABLE
#include <SDL2/SDL.h>

#ifdef AVIS_WIN32_CONSOLE_ENABLE
  #include <iostream>
  #include <cstdio>

  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #include <shellapi.h>
#endif

#include "logging.hpp"

#include <memory>
#include <utility>
#include <functional>
#include <boost/noncopyable.hpp>

namespace avis
{

// SDL Window base abstract class.
class Window
{
public:
  virtual ~Window() = 0;

  // Copying windows is prohibited.
  Window(Window const&) = delete;
  Window & operator=(Window const&) = delete;

  // Moving windows is allowed.
  Window(Window && other) noexcept
  {
    std::swap(_ptr, other._ptr);
  }

  Window & operator=(Window && other) noexcept
  {
    if (this != &other)
    {
      destroy(); //?
      std::swap(_ptr, other._ptr);
    }
    return *this;
  }

  // Get the internal SDL window index.
  Uint32 id() const
  {
    return SDL_GetWindowID(_ptr);
  }

protected:
  // Get the internal pointer.
  SDL_Window * get_pointer() const noexcept { return _ptr; }

  // Destroy this window.
  void destroy()
  {
    if (_ptr)
    {
      SDL_DestroyWindow(_ptr);
      _ptr = nullptr;
    }
  }

private:
  // The pointer to the SDL window struct.
  SDL_Window * _ptr = nullptr;
};


// The starting point: top level SDL wrapper.
class SDL: boost::noncopyable
{
public:
  // SDL init flags (select subsystems to initialize).
  // http://wiki.libsdl.org/SDL_InitSubSystem#Remarks
  enum class Init: Uint32
  {
    Timer = SDL_INIT_TIMER,
    Audio = SDL_INIT_AUDIO,
    Video = SDL_INIT_VIDEO,
    Joystick = SDL_INIT_JOYSTICK,
    Haptic = SDL_INIT_HAPTIC,
    Gamecontroller = SDL_INIT_GAMECONTROLLER,
    Events = SDL_INIT_EVENTS,
    Everything = SDL_INIT_EVERYTHING
  };

  // SDL initialization.
  explicit SDL(Init flags)
  {
    if (++_instances == 1)
    {
      #ifdef AVIS_WIN32_CONSOLE_ENABLE
      // Either attach to a parent console or create a separate Win32 console 
      // in order to enable standard in/out streams.
      if (
      #ifdef AVIS_WIN32_CONSOLE_ATTACH_PARENT
        AttachConsole(ATTACH_PARENT_PROCESS) ||
      #endif
        AllocConsole())
      {
        freopen("CONIN$", "r", stdin);
        freopen("CONOUT$", "w", stderr);
        freopen("CONOUT$", "w", stdout);
        cin.sync();
        cout.flush();
        clog.flush();
      }
      else // allocating console failed.
      {
        AVIS_LOG("Allocating console failed with error code ", GetLastError(), '\n');
      }
      #endif

      // http://wiki.libsdl.org/SDL_Init
      auto const e = SDL_Init((Uint32)flags);
      AVIS_ASSERT(e == 0, "SDL_Init failure, ", SDL_GetError());
    }
    else // I want every subsystem to be initialized only once,
    {    // no matter how many times SDL has been created.
      (*this)(flags);
    }
  }

  // Initialize everything.
  SDL(): SDL(Init::Everything) {}

  // Quit SDL.
  ~SDL()
  {
    if (--_instances == 0)
    {
      // http://wiki.libsdl.org/SDL_Quit
      SDL_Quit();
    }
  }

  // Call this to init SDL subsystems.
  SDL & operator()(Init flags)
  {
    // I want every subsystem to be initialized only once,
    // no matter how many times SDL has been created.
    static Uint32 const subsystems[]
    {
      SDL_INIT_TIMER,
      SDL_INIT_AUDIO,
      SDL_INIT_VIDEO,
      SDL_INIT_JOYSTICK,
      SDL_INIT_HAPTIC,
      SDL_INIT_GAMECONTROLLER,
      SDL_INIT_EVENTS,
    };

    auto const systems_left = (Uint32)flags &~ SDL_WasInit(0);
    for (auto system: subsystems)
    {
      if (systems_left & system)
      {
        // http://wiki.libsdl.org/SDL_InitSubSystem
        auto const e = SDL_InitSubSystem(system);
        AVIS_ASSERT(e == 0, "SDL_InitSubSystem(", system, ") failure, ", SDL_GetError());
      }
    }
    
    return *this;
  }

  // Delay for ms milliseconds.
  void delay(Uint32 ms) const
  {
    // http://wiki.libsdl.org/SDL_Delay
    SDL_Delay(ms);
  }

private:
  inline static size_t _instances = 0;
};

// SDL::Init values may be combined with | operator.
inline SDL::Init operator|(SDL::Init a, SDL::Init b)
{
  return SDL::Init{(Uint32)a | (Uint32)b};
}

inline SDL::Init & operator|=(SDL::Init & a, SDL::Init b)
{
  return a = a | b;
}

}

#endif//AVIS_SIDEROS_HPP_INCLUDED_Q54UP0
