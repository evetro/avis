Min
===

Minimal SDL programs.

This does not mean that these are absolutely minimal. They are intended to represent some aspects of initialization/finalization of SDL structures.

Contents:

* min.0.cpp: just creates a window, fills it with color and exits after a delay;
* min.1.cpp: min.0 + Win32 console with std in/out;
* min.2.cpp: min.1 + avis/logging.hpp;
* ... this list is going to grow in future.
