Version history
===============

## 0.x

### 0.1.0 == 2020 Jan 05

\+ Min subproject added (min.0.cpp, min.1.cpp).


### 0.0.0 == 2020 Jan 05

Initial empty version.
