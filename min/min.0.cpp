// Avis/min.0.cpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
#include <SDL2/SDL.h>

// Constants.
int const WINDOW_WIDTH  = 640;
int const WINDOW_HEIGHT = 480;

// Global state.
SDL_Window  * window;  // our sole window.
SDL_Surface * surface; // "surface" contains the picture that is shown in the window.

// Entry point.
int main(int argc, char* args[])
{
  // Initialization.
  // http://wiki.libsdl.org/SDL_Init
  SDL_Init(SDL_INIT_VIDEO);
  // http://wiki.libsdl.org/SDL_CreateWindow
  window = SDL_CreateWindow("min.0",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, 
      WINDOW_WIDTH, 
      WINDOW_HEIGHT,
      SDL_WINDOW_SHOWN);
  if (!window)
    return 1; // failed to create the window.
  // http://wiki.libsdl.org/SDL_GetWindowSurface
  surface = SDL_GetWindowSurface(window);
  if (!surface)
    return 2; // failed to get the surface of the window.

  // Fill the window with some color...
  // http://wiki.libsdl.org/SDL_FillRect
  SDL_FillRect(surface, nullptr, // http://wiki.libsdl.org/SDL_MapRGB
      SDL_MapRGB(surface->format, 0x4F, 0x00, 0xEE));
  // http://wiki.libsdl.org/SDL_UpdateWindowSurface
  SDL_UpdateWindowSurface(window);
  // ... and wait for two seconds.
  // http://wiki.libsdl.org/SDL_Delay
  SDL_Delay(2000);
  
  // We are done, finalizing.
  // http://wiki.libsdl.org/SDL_DestroyWindow
  SDL_DestroyWindow(window);
  // http://wiki.libsdl.org/SDL_Quit
  SDL_Quit();
  return 0;
}
