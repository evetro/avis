// Avis/min.3.cpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)

// To enable Win32 console attachment:
//#define AVIS_WIN32_CONSOLE_ENABLE
#define AVIS_LOG_FILENAME "min.3.log"
#include <avis/sideros.hpp>
using namespace avis;

// Constants.
int const WINDOW_WIDTH  = 640;
int const WINDOW_HEIGHT = 480;

// Global state.
SDL_Window  * window;  // our sole window.
SDL_Surface * surface; // "surface" contains the picture that is shown in the window.

// Entry point.
int main(int argc, char* args[])
{
  // Initialization.
  AVIS_LOG(avis::now, "\nmin.3 started\n");
  SDL sdl(SDL::Init::Video);

  // http://wiki.libsdl.org/SDL_CreateWindow
  window = SDL_CreateWindow("min.3",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, 
      WINDOW_WIDTH, 
      WINDOW_HEIGHT,
      SDL_WINDOW_SHOWN);
  AVIS_ASSERT(window, "SDL_CreateWindow failure, ", SDL_GetError());

  // http://wiki.libsdl.org/SDL_GetWindowSurface
  surface = SDL_GetWindowSurface(window);
  AVIS_ASSERT(surface, "SDL_GetWindowSurface failure, ", SDL_GetError());

  // Fill the window with some color...
  // http://wiki.libsdl.org/SDL_FillRect
  SDL_FillRect(surface, nullptr, // http://wiki.libsdl.org/SDL_MapRGB
      SDL_MapRGB(surface->format, 0x4F, 0x00, 0xEE));
  // http://wiki.libsdl.org/SDL_UpdateWindowSurface
  SDL_UpdateWindowSurface(window);
  // ... and wait for two seconds.
  sdl.delay(2000);
  
  // We are done, finalizing.
  // http://wiki.libsdl.org/SDL_DestroyWindow
  SDL_DestroyWindow(window);
  AVIS_LOG(avis::now, "\nmin.3 finished\n");
  return 0;
}
