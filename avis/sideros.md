Sideros
=======

A wrapper library for SDL components.

Define:

* AVIS_WIN32_CONSOLE_ENABLE to create a Win32 console in SDL constructor;
* and AVIS_WIN32_CONSOLE_ATTACH_PARENT to try to attach to console of the parent process.


SDL
---

Top-level class. Its constructors initialize SDL subsystems.
It is non-copyable. My intention is that there must be only one SDL object. Currently, it is possible to create any amount of instances of SDL, but SDL subsystems are intialized maximum once.

SDL constructors and destructor are not thread-safe.

Members:

* Init is an enum class which may be OR'ed with | and |= operators.
* SDL(Init flags): select, which SDL subsystems are to be initialized.
* SDL(): initializes everything.
* ~SDL(): calls SDL_Quit when the last SDL instance is destroyed.
* delay(ms): delays execution for ms milliseconds.
