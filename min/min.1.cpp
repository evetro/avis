// Avis/min.1.cpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
#include <SDL2/SDL.h>
#include <iostream>
#include <cstdio>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h>

using namespace std;

// Constants.
int const WINDOW_WIDTH  = 640;
int const WINDOW_HEIGHT = 480;

// Global state.
SDL_Window  * window;  // our sole window.
SDL_Surface * surface; // "surface" contains the picture that is shown in the window.

// Entry point.
int main(int argc, char* args[])
{
  // Either attach to a parent console or create a separate Win32 console 
  // in order to enable standard in/out streams.
  if (AttachConsole(ATTACH_PARENT_PROCESS) || AllocConsole())
  {
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);

    cin.sync();
    cout.flush();
    clog.flush();

    // Testing standard input/output.
    cout << "Hello there!\n";
    clog << "No errors.\nEnter some number: ";
    int x = 100;
    cin >> x;
    cout << "you entered: " << x << endl;
  }

  // Initialization.
  // http://wiki.libsdl.org/SDL_Init
  SDL_Init(SDL_INIT_VIDEO);

  // http://wiki.libsdl.org/SDL_CreateWindow
  window = SDL_CreateWindow("min.1",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, 
      WINDOW_WIDTH, 
      WINDOW_HEIGHT,
      SDL_WINDOW_SHOWN);
  if (!window)
    return 1; // failed to create the window.
  // http://wiki.libsdl.org/SDL_GetWindowSurface
  surface = SDL_GetWindowSurface(window);
  if (!surface)
    return 2; // failed to get the surface of the window.

  // Fill the window with some color...
  // http://wiki.libsdl.org/SDL_FillRect
  SDL_FillRect(surface, nullptr, // http://wiki.libsdl.org/SDL_MapRGB
      SDL_MapRGB(surface->format, 0x4F, 0x00, 0xEE));
  // http://wiki.libsdl.org/SDL_UpdateWindowSurface
  SDL_UpdateWindowSurface(window);
  // ... and wait for two seconds.
  // http://wiki.libsdl.org/SDL_Delay
  SDL_Delay(2000);
  
  // We are done, finalizing.
  // http://wiki.libsdl.org/SDL_DestroyWindow
  SDL_DestroyWindow(window);
  // http://wiki.libsdl.org/SDL_Quit
  SDL_Quit();
  return 0;
}
