Logging
=======

A minimalistic sub-library avis/logging.hpp for logging and assertions.
Logging destination is defined by macros

* AVIS_LOG_TO_STDERR: if enabled then logging is directed to stderr;
* AVIS_LOG_FILENAME: otherwise logging is directed to file, which name is defined by this macro.

If neither AVIS_LOG_TO_STDERR, nor AVIS_LOG_FILENAME are defined then logging.hpp defines AVIS_LOG_FILENAME = "avis.log".

If AVIS_LOG_TO_STDERR is not defined and AVIS_LOG_FILE_APP is defined then the log file is opened in append mode.

Logging defines the following macros:

* AVIS_LOG(...) outputs everything to the current log;
* AVIS_LOG_DBG(...) same as AVIS_LOG but it is disabled if NDEBUG is defined;
* AVIS_ASSERT(c, ...) if c is false, outputs an error message (including all other parameters) and exits with return code -1 using std::exit (thus, all destructors are executed as normal);
* AVIS_CHECK(c, ...) if c is false, outputs an error message;
* AVIS_CHECK_SLOW(c, ...) same as AVIS_CHECK but is enabled only if AVIS_CHECK_SLOW_ENABLE or AVIS_CHECK_VERY_SLOW_ENABLE are defined;
* AVIS_CHECK_VERY_SLOW(c, ...) same as AVIS_CHECK but is enabled only if AVIS_CHECK_VERY_SLOW_ENABLE is defined.

Each logging output operation is guarded by a mutex.

Logging is done using the global noncopyable object avis::log (its operator()(...)).
Logging output stream state may be checked by avis::log::is_valid().
Logging may be disabled with avis::log::off() and enabled with avis::log::on().

It is possible to print the current local date, time or date and time by sending avis::current_date, avis::current_time, avis::now respectively to an output string (including the log):

AVIS_LOG(avis::now, '\n'); // print the current local date and time and newline character to the log.
