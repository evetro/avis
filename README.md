Avis
====

Version 0.1.0

I've created this repository to place some SDL-based programs here. The original intent was to reimplement some programs from my previous similar project AV Orchid (2012--2020) as well as to make some more algorithm visualizers (hence name "Avis"). I'll see how it will go.

The project is being developed in [Visual Studio Code](https://code.visualstudio.com/) with [MinGW distro](https://nuwen.net/) by Stephan T. Lavavej. This distro includes [SDL2](http://www.libsdl.org/) library as well as some other libraries (e.g. [Boost](https://www.boost.org/)) that I may use. I may also use [Eigen](http://eigen.tuxfamily.org/) and [CGAL](https://www.cgal.org/). The project serves mostly educational purposes.

Contents:

* .vscode folder contains settings for Visual Studio Code. These are written for my setup, where MinGW is placed aside the working folder. Each .cpp file is considered a separate application, which is built into a separate .exe by the build task. There is no "all rebuild" though, as I do not need it (you may read more on this [here](https://evetro.wordpress.com/2020/01/05/vscode_sdl/) -- in Russian);
* avis folder contains shared code;
* min folder contains minimal SDL program versions;
* ... this list will grow in the next version.
