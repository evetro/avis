// Avis/min.2.cpp
// Copyright Kuvshinov D.R. 2020
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
// To enable Win32 console attachment:
//#define AVIS_WIN32_CONSOLE_ENABLE
#include <SDL2/SDL.h>

#ifdef AVIS_WIN32_CONSOLE_ENABLE
#include <iostream>
#include <cstdio>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h>
#endif

#define AVIS_LOG_FILENAME "min.2.log"
#include <avis/logging.hpp>

using namespace std;

// Constants.
int const WINDOW_WIDTH  = 640;
int const WINDOW_HEIGHT = 480;

// Global state.
SDL_Window  * window;  // our sole window.
SDL_Surface * surface; // "surface" contains the picture that is shown in the window.

// Entry point.
int main(int argc, char* args[])
{
  #ifdef AVIS_WIN32_CONSOLE_ENABLE
  // Either attach to a parent console or create a separate Win32 console 
  // in order to enable standard in/out streams.
  if (AttachConsole(ATTACH_PARENT_PROCESS) || AllocConsole())
  {
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stderr);
    freopen("CONOUT$", "w", stdout);
    cin.sync();
    cout.flush();
    clog.flush();        
    cout << "Hello there!\n";
  }
  else // allocating console failed.
  {
    AVIS_LOG("Allocating console failed with error code ", GetLastError(), '\n');
    // Proceed or exit.
  }
  #endif

  // Initialization.
  AVIS_LOG(avis::now, "\nmin.2 started\n");
  // http://wiki.libsdl.org/SDL_Init
  int e = SDL_Init(SDL_INIT_VIDEO);
  AVIS_ASSERT(e == 0, "SDL_Init failure, ", SDL_GetError());

  // http://wiki.libsdl.org/SDL_CreateWindow
  window = SDL_CreateWindow("min.2",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, 
      WINDOW_WIDTH, 
      WINDOW_HEIGHT,
      SDL_WINDOW_SHOWN);
  AVIS_ASSERT(window, "SDL_CreateWindow failure, ", SDL_GetError());

  // http://wiki.libsdl.org/SDL_GetWindowSurface
  surface = SDL_GetWindowSurface(window);
  AVIS_ASSERT(surface, "SDL_GetWindowSurface failure, ", SDL_GetError());

  // Fill the window with some color...
  // http://wiki.libsdl.org/SDL_FillRect
  SDL_FillRect(surface, nullptr, // http://wiki.libsdl.org/SDL_MapRGB
      SDL_MapRGB(surface->format, 0x4F, 0x00, 0xEE));
  // http://wiki.libsdl.org/SDL_UpdateWindowSurface
  SDL_UpdateWindowSurface(window);
  // ... and wait for two seconds.
  // http://wiki.libsdl.org/SDL_Delay
  SDL_Delay(2000);
  
  // We are done, finalizing.
  // http://wiki.libsdl.org/SDL_DestroyWindow
  SDL_DestroyWindow(window);
  // http://wiki.libsdl.org/SDL_Quit
  SDL_Quit();
  AVIS_LOG(avis::now, "\nmin.2 finished\n");
  return 0;
}
